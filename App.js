import React , {useState} from 'react';
import {TouchableOpacity , Text , Image , View,ActivityIndicator,StatusBar} from 'react-native';
import Tflite from 'tflite-react-native'
import ImagePicker from 'react-native-image-picker'
import Modal from 'react-native-modal'
import GestureRecognizer from 'react-native-swipe-gestures'


const App = () => {
  const [image,setImage] = useState('')
  const [result,setResult] = useState([])
  const [visible,setVisible] = useState(false)
  const [loading,setLoading] = useState(false)

  StatusBar.setBarStyle('light-content')
  StatusBar.setBackgroundColor('#7a00e5')

  const showPicker = ()=>{
        const options = {
          title: 'انتخاب عکس',
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
          takePhotoButtonTitle:'بازکردن دوربین',
          cancelButtonTitle:'لغو',
          chooseFromLibraryButtonTitle:'انتخاب از گالری'
        };
        
        ImagePicker.showImagePicker(options, (response) => {

          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            setLoading(true)
            setImage(response.uri)
            let tensorflow = new Tflite();
            tensorflow.loadModel({
              // model : 'data/ssd_mobilenet.tflite',
              // labels : 'data/ssd_mobilenet.txt',
              model : 'data/yolov2_tiny.tflite',
              labels : 'data/yolov2_tiny.txt',
              numThreads : 4
            },(err,res)=>{
              if(err){
                alert('خطا هنگام خواندن اطلاعات')
              }
              else{
                tensorflow.detectObjectOnImage({
                  path: 'file://' + response.path,
                  model:'YOLO',
                  threshold: 0.0,
                  numResultsPerClass: 1,
                },(err,res)=>{
                  setLoading(false)
                  if (err) console.log('error',err)
                  else{
                    console.log(res)
                    setResult(res)
                    setVisible(true)
                  }
                })
              }
        });
      }
    })
    
  }
  return (
    <GestureRecognizer config={{velocityThreshold:0.3,directionalOffsetThreshold:80}} style={{flex:1}} onSwipeUp={()=>setVisible(true)} onSwipeDown={()=>setVisible(false)} >
    
    <View style={{flex:1,backgroundColor:'#8500fa'}}>
    <Text style={{fontSize:17,fontFamily:'IRANSansMobile',color:'#fff',textAlign:'center',margin:14}}>تشخیص اشیاء</Text>
    <TouchableOpacity activeOpacity={1} onPress={()=>showPicker()} style={{flex:1,elevation:10,flexDirection:'row',backgroundColor:'#eee',justifyContent:'center',alignItems:'center',borderTopLeftRadius:18,borderTopRightRadius:18}}>
    {image===''?
    <View style={{flex:1,alignItems:'center'}}>
    <Text style={{fontSize:15,fontFamily:'IRANSansMobile',color:'#fff',backgroundColor:'#8500fa',borderRadius:50,padding:10,paddingBottom:35,paddingTop:35,elevation:10,textAlign:'center'}}>
      انتخاب عکس
    </Text>
    <Text style={{fontSize:12,fontFamily:'IRANSansMobile',color:'#666',padding:10,textAlign:'center',position:'absolute',top:120}}>
      پروژه کارشناسی تشخیص اجسام - علی شرقی 
    </Text>
    </View>
    :
    <Image resizeMode={'contain'} style={{flex:1,borderRadius:18,margin:16}} width={'95%'} height={'95%'} source={{uri:image}} />}
    </TouchableOpacity>
    <Modal
      isVisible={visible}
      hasBackdrop={true}
      backdropOpacity={0.2}
      onBackdropPress={()=>setVisible(false)}
      onBackButtonPress={()=>setVisible(false)}
      style={{justifyContent: 'flex-end',margin: 0}}>
      <View style={{backgroundColor:'#fff',justifyContent:'center',borderTopLeftRadius:18,borderTopRightRadius:18,padding:8}}>
      {
        result.length>0?
        result.map((value,index)=>{
          if (index>4||value.confidenceInClass<0.20) return
          return(
          <Text style={{opacity:1-(index/5),color:'#222',fontSize:14,margin:4,fontFamily:'IRANSansMobile'}} key={index.toString()}>{value.detectedClass + ' : ' + (value.confidenceInClass*100).toFixed(1)+'%'}</Text>
        )}):<Text style={{color:'#222',fontSize:14,margin:4,fontFamily:'IRANSansMobile'}}>چیزی پیدا نکردم :(</Text>
      }
      </View>
    </Modal>
    <Modal
      isVisible={loading}
      hasBackdrop={true}
      onBackdropPress={()=>setLoading(false)}
      onBackButtonPress={()=>setLoading(false)}
      backdropOpacity={0.2}
      style={{alignSelf:'center',bottom:'40%'}}>
      <View style={{backgroundColor:'#fff',borderRadius:18,padding:8}}>
      <ActivityIndicator size={'large'} color={'orange'} animating={loading} />
      </View>
    </Modal>
    </View>
    </GestureRecognizer>
  );
};

export default App;
