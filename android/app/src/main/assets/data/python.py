import tensorflow as tf
conv = tf.lite.Interpreter(model_path="./yolov2_tiny.tflite")
conv.allocate_tensors()
print("conv.get_input_details(): {}".format(conv.get_input_details()))
print("conv.get_output_details(): {}".format(conv.get_output_details()))
print("get_tensor_details(): {}".format(conv.get_tensor_details()))

# input = conv.tensor(conv.get_input_details()[0]["index"])
# output = conv.tensor(conv.get_output_details()[0]["index"])
# for i in range(10):
# input().fill(3.)
# conv.invoke()
# print("inference %s" % output())

# print(conv.allocate_tensors())